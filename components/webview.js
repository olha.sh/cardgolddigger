import React from 'react'
import { WebView } from 'react-native-webview';

const WebViewExample = (props) => {

   return (
   <WebView source ={{ uri: `${props.link}`} }
         />
   )
}

export {
    WebViewExample
} 