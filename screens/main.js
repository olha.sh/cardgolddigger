import React from 'react'

import{WebViewExample} from '../components/webview'

function MainScreen(props) {
  return (
    <WebViewExample link = {props.value}/>
  );
}

export{
    MainScreen
}