import React  from 'react';
import { View, Text} from 'react-native';

class WelcomeScreen extends React.Component{ 
 
render() {
  return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Welcome </Text>
      </View>
    );
  } 
}

export {
    WelcomeScreen
}