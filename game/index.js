import React, {useState} from 'react';
import {
  StyleSheet,
} from 'react-native';
import {StartScreen} from './screen/startScreen'
import {CardGame} from './screen/cardGame'
import {FinScreen} from './screen/finScreen'

function RepeatCard(){
const [isPlay, setIsPlay] = useState(false)
const [win, setWin] = useState(false)
const [level, setLevel] = useState(1);
const [score, setScore] = useState(0);

function nextLevel(){
    setLevel(()=> level +1)
    setWin(false)
}

return(
    <> 
        {(isPlay == false) ? 
            ( <StartScreen 
                closeScreen={()=>setIsPlay(true)}
            />
        ): (
            (win === true)? <FinScreen 
                handleChangeLevel={nextLevel}
                value={score}/> :
            <CardGame 
                value={score}
                setValue={setScore}
                openWinScren={setWin}
                lvlGame={level}/>
        )}
       

    </>
 
)


}

export{
    RepeatCard
}