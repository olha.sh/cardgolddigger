
import React from 'react';
import {
    Text,
    ImageBackground,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

function StartScreen({closeScreen}){
    return(
        <ImageBackground
            source={require('../../assets/image/BGstart.png')} 
            style={styles.fonImage}
            resizeMode='cover'
        >
            <TouchableOpacity 
                onPress={closeScreen}
                style={styles.btn}
            >
                <ImageBackground  
                    source={require('../../assets/image/startBtn.png')} 
                    style={styles.btn}
                    resizeMode='contain'
                >
                    <Text style={styles.text}>
                        Play
                    </Text>
                </ImageBackground>
            </TouchableOpacity>
        </ImageBackground>
    )
}

styles=StyleSheet.create({
    fonImage: {
        height: "100%",
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center'
      },
    btn: {
        width: 250,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',  
    },
    text: {
        color: '#fff',
        fontSize: 25,
        textTransform: 'uppercase'
    }
})

export{
    StartScreen
}

