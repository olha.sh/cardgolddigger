import React from 'react';
import {
  Text,
  ImageBackground,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

function FinScreen({handleChangeLevel, value}){
  return(
    <ImageBackground 
      source={require('../../assets/image/BGfin.png')}
      style={styles.fonContainer}
      resizeMode='cover'
    >
      <Text style={styles.scoreTxt}>
        Score {value}
      </Text>

      <TouchableOpacity 
                onPress={handleChangeLevel}
                style={styles.btn}
            >
                <ImageBackground  
                    source={require('../../assets/image/startBtn.png')} 
                    style={styles.btn}
                    resizeMode='contain'
                >
                    <Text style={styles.text}>
                        Next Level
                    </Text>
                </ImageBackground>
            </TouchableOpacity>

    </ImageBackground>
  )
}

const styles=StyleSheet.create({
  fonContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems:'center'
  },
  btn: {
    width: 250,
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',  
    marginBottom: 60
},
text:{
  textTransform: 'uppercase',
  color: '#fff',
   fontSize: 25,
   fontWeight: "700"
},
scoreTxt: {
  marginBottom: 35,
  color: '#fff758',
  fontSize: 25,
  fontWeight: "700",
  textTransform: 'uppercase',
}
})

export{
  FinScreen
}