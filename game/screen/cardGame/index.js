
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
} from 'react-native';
import MemoryGame from './gameComponent/memoryGame'
import {HeaderScore} from './gameComponent/headerScore'
import {cards} from '../../assets/data'


function CardGame({openWinScren, lvlGame, value, setValue}) {

  const [cardsState, setCardsState] = useState(cards);
  const [clicked, setClicked] = useState(false)
  const [openCards, setOpenCards] = useState(false)
  const [modalVisible, setModalVisible] = useState(false);

  const [cardName, setCardName] = useState(null);
  
  const [counter, setCounter] = useState(0)





 function handlOpenCard(){
  const newArr = [];

  setCardsState(shuffleArray(cards))

  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = true;
  });

  setCardsState(newArr)
  return newArr
}

function handlCloseCard(){
  const newArr = [];
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = false;
  });
  setCardsState(newArr)
  setClicked(true)
  return newArr
}
function openOneCard (id){
  const newArr = [];
  cardsState.forEach((item, index) => {
    newArr[index] = item;
      if( id === newArr[index].id){
        newArr[index].isOpen = true; 
      }
    })
    setCardsState(newArr)
}

function handlChooseCard (id, name) {
  console.log('SCORE' + value)

  openOneCard(id)
  setCardName(name)
  

 
  if(counter==1 && cardName == name){
    setValue(()=>value+1)
    setCounter(()=>counter-1)
    if(value == 5){
      openWinScren(true)
      console.log('YOU WIN')
    }
    
  }
  if(counter==1 && cardName != name){
    setTimeout(()=>{
      handlCloseCard()
      setValue(0)
    },1000)
    
    setCounter(()=>counter-1)
  }
   if (counter==0){
    setCardName(name);
    setCounter(()=>counter+1)
  }
  

 
}

function shuffleArray (arr) {
  let newArr = arr.sort(() => Math.random() - 0.5)
  return newArr
};


useEffect(()=>{
  
  
  const openTimer =  setTimeout(()=>{
      handlOpenCard()
    }, 2000)
    
 const openCard = setTimeout(()=>{
      handlCloseCard()
    }, 5000)
    
    return () => {
      clearTimeout(openTimer, openCard)
    }
  
}, [])

  return (
     <ImageBackground 
        source={require('../../assets/image/gameBG.png')} 
        style={styles.fonImage}
     >
      <View style={styles.container}>
        <HeaderScore
            score = {value}
            level = {lvlGame}
          />
        <MemoryGame
          cards={cardsState}
          canClick={clicked}
          handleOpen={(value)=> setOpenCards(!value)}
          handleCard={handlChooseCard}
        />
      </View>
     </ImageBackground>      
  );
};

const styles = StyleSheet.create({
  fonImage: {
    height: "100%",
    width: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
   
  }
});

export {
   CardGame
 
} 