import React from 'react';
import {StyleSheet, ImageBackground, View} from 'react-native'
import Card from '../card'
function MemoryGame({cards,handleCard, canClick}){
    return(
    <ImageBackground 
        source={require('../../../../assets/image/cardContainer.png')} 
        style={styles.fonContainer}
        resizeMode='contain'
    >
        <View style={styles.wrapperCard}>
        {cards.map((item)=> {
            return  (
        <Card 
            click={canClick}
            nameCard={item.name}
            src={item.src} 
            key={item.id}
            openCard={item.isOpen}
            idKey={item.id}
            chooseCard={handleCard}
        />
        )
    })} 
        </View> 
    </ImageBackground>
    )
}

const styles = StyleSheet.create({

    
    fonContainer: {
        width: 400,
        height: 400,
        marginTop: 50,
        position: 'relative',
        justifyContent: 'center',
        alignItems:'center'
      },
    wrapperCard: {
        width: 260,
        height: 260,
        position: 'absolute',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
       
        
    }
})


export default MemoryGame
