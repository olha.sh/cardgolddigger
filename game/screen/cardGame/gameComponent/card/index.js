import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native'


function Card({src, openCard, idKey, chooseCard, nameCard}){

    return(
        <View
            style={styles.card} 
            key={idKey} 
        >
            {openCard ? (
                <TouchableOpacity onPress={()=>chooseCard(idKey, nameCard)}  key={idKey} >
                    <Image source={src} 
                    style={styles.fontImg}
                    /> 
                </TouchableOpacity>
                    ) : 
                (
                    <TouchableOpacity onPress={()=>chooseCard(idKey, nameCard)}  key={idKey} >
                        <Image  source={require('../../../../assets/image/back.png')} 
                        style={ openCard ? styles.flipBack : styles.backImg}
                    /> 
                    </TouchableOpacity>
                )}
        </View>   
        )
}
const styles = StyleSheet.create({
    card:{
        width: 68,
        height: 60,
        marginBottom: 5,
    },
    flipBack :{
        display: 'none'
    },
    fontImg: {
        width: 68,
        height: 60,
        // margin: 3,
    },
    backImg: {
        width: 68,
        height: 60,
        
      }
})




export default Card