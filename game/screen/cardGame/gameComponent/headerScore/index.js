import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
} from 'react-native';

function HeaderScore ({score, level}){
    return(
     <View style={styles.headerContainer}>
       <View style={styles.headWrapper}>
         <Image 
            source={require('../../../../assets/image/gold.png')}
            style={styles.imgGold}
          />
         <Text style={styles.scoreTxt}>
           {score}
        </Text>
       </View>
       
      <Text style={styles.levelTxt}>
      Lv {level}
      </Text>
     </View>
      
    )
}

const styles = StyleSheet.create({
  headerContainer:{
    padding: 20,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headWrapper:{
    flexDirection: 'row',
    alignItems:'center'
  },
  imgGold: {
    width: 45,
    height: 45,
  },
  scoreTxt: {
    marginLeft: 10,
    color: '#fff',
    fontSize: 30,
    fontWeight: '700'
  },
  levelTxt: {
    color: '#122158',
    fontSize: 30,
    fontWeight: '700'
  }

})

export{
    HeaderScore
}