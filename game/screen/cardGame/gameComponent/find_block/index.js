import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';

function FindBlock({src}){


    return(
        <View style={styles.wrapper}>
            <Text style={{ fontSize: 20 , color: '#fcff03'}}>
                Find this card: 
            </Text>
            <Image style={styles.image} 
                source={src}/>
        </View>
    )
}

const styles= StyleSheet.create({
wrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
},
image: {
    width: 40,
    height: 40,
    marginLeft: 20
},
desc: {
    fontSize: 40,
    fontWeight: "bold",
    color: 'red',
    fontWeight: "bold",
}

})

export{
    FindBlock
}
